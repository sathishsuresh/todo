import React from 'react'
import {View,Text, SafeAreaView, TouchableOpacity, TextInput,Image} from 'react-native'
import { BottomSheet } from 'react-native-btr';
import {GoogleSignin,GoogleSigninButton,statusCodes} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'
import { FlatList } from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import {connect} from 'react-redux'
import { addTodo, deleteTodo } from '../redux/actions';
// import { firebase } from "@react-native-firebase/auth";
// import { initializeApp } from '@firebase/app';
// import * as database from 'firebase/database'
// import * as firebase from 'firebase/database'

const firebaseConfig = {
    apiKey: "AIzaSyDERAMtTUciZDaHWP42XnuGZ_vHJzIiS_E",
    authDomain: "test-a2c68.firebaseapp.com",
    projectId: "test-a2c68",
    storageBucket: "test-a2c68.appspot.com",
    // messagingSenderId: "162014228232",
    // appId: "1:162014228232:web:d6193a5f090188df8ec6bd",
    // measurementId: "G-RFDEPPFMWL"
  };

 class Home extends React.Component{   
    componentDidMount(){
        // this.readuserdata()
    }
    constructor(props) {
        super(props);
      
        this.state = {
            
         Model_Visibility:false,
         Model_View_Visibility:false,
         name:'',
         Todo_array:[],
         todo_list:[]

        }
    }
     handleAddTodo = () => {
        this.props.addTodo(this.state.name)
        this.setState({Model_Visibility:false})
        
      }
    signOut = async () => {
        try {
          await GoogleSignin.revokeAccess();
          await GoogleSignin.signOut();
          auth().signOut().then(()=>Toast.show("Logout successfully!", Toast.SHORT))            
            this.props.navigation.navigate('Login')
        } catch (error) {
          console.error('error ==',error);
        }
      };

      writeuserdata(todo){
          console.log("fun",todo)          
        database().ref('TodoEntry').push ({
            todo
        })
        this.setState({Model_Visibility:false})
      }
      getListViewItem = (item) => {  
        alert(item.name);  
    } 
    readuserdata=()=>{
        this.database= database().ref().child('TodoEntry')
        this.database.on("value",(snap)=>{
            let dummyarray=[]
            snap.forEach(child=>{
                dummyarray.push({
                    name: child.val().todo
                })
            })
            this.setState({Todo_array:dummyarray})
            console.log('fbbbdata',this.state.Todo_array)
        })
    }
    render(){
        return(
            <SafeAreaView  style={{flex:1,backgroundColor:'white'}}>               
               <TouchableOpacity 
               style={{width:'30%',backgroundColor:'red',marginTop:'5%',marginStart:5,borderRadius:8,height:30,alignItems:'center'}}
               onPress={()=>this.signOut()}>
               <View  >
                    <Text style={{alignItems:'center',alignSelf:'center',color:'#fff',margin:5}} >LogOut</Text>
               </View>
               </TouchableOpacity>
                <TouchableOpacity 
                onPress={() => this.setState({ Model_Visibility: true,Model_View_Visibility:true                     
                    })}
                style={{width:50,height:45,borderRadius:45,backgroundColor:'red',alignSelf:'flex-end',marginEnd:'5%',marginTop:'2%',height:'8%',}}>                                   
                    <Image
                    source={require('../assets/plus.png')}
                    style={{width:20,height:20,alignSelf:'center',justifyContent:'center',margin:10,tintColor:'#fff'}}
                    />
            </TouchableOpacity>
            <Text style={{alignSelf:'center',justifyContent:'center',fontWeight:'bold',fontSize:25,color:'#000'}} > ToDo List</Text>
            <FlatList  
                    // data={this.props.toDoList}  
                    data={this.props.todo_list[0]}  
                    renderItem={({item}) =>  
                        <Text style={{padding: 10,  
                            fontSize: 18,  
                            height: 44, }}  
                              onPress={this.getListViewItem.bind(this, item)}>{item.task}</Text>}  
                    ItemSeparatorComponent={this.renderSeparator}  
                /> 

            <BottomSheet
                     visible={this.state.Model_Visibility}
                   
                    onBackButtonPress={() => this.setState({ Model_Visibility: false,Model_View_Visibility:false,})}
                    onBackdropPress={() => this.setState({ Model_Visibility: false,Model_View_Visibility:false,})}
                >

                    {this.state.Model_View_Visibility &&
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
                        <View style={{
                            width: '90%',
                           height:'50%',
                            flexDirection: 'column', alignSelf: 'center',
                            backgroundColor: 'white',
                            justifyContent: 'center',                          
                        }}>
                            <View style={{
                                height:'8%',
                                alignItems: 'flex-end', justifyContent: 'flex-end'
                            }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ Model_Visibility: false })
                                       
                                    }}                                
                                >
                                     <Image
                    source={require('../assets/close.png')}
                    style={{width:35,height:35,alignSelf:'center',justifyContent:'center',margin:10,}}
                    />                                 
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: '20%',alignItems: 'center', justifyContent: 'center',}}>
                            <Text style={{fontFamily:'OpenSans',fontSize:16, justifyContent:'center', alignItems:'center', color:'#797979'}}>Please Enter Todo</Text>
                            </View>
                            <View style={{ width: '100%', height: 50, }}>
                        <View style={{
                            width: '80%', height: 50, backgroundColor: 'lightgrey',
                            alignSelf: 'center', justifyContent: 'center', marginTop: 20, borderRadius: 10
                        }}>
                            <TextInput style={{ marginLeft: 20, fontSize: 16 ,fontFamily:'OpenSans'}} 
                                placeholderTextColor="black" 
                                onChangeText={value => this.setState({ name:value })}
                                // value={this.state.forgot_email}
                                // onChangeText={(user_value) => this.forgot_email_validation(user_value)}
                                >
                            </TextInput>
                        </View>
                        <TouchableOpacity 
               style={{width:'30%',backgroundColor:'red',marginTop:'5%',marginStart:5,borderRadius:8,height:30,alignItems:'center',alignSelf:'center'}}
            //    onPress={()=>this.writeuserdata(this.state.name)}
            onPress={()=>this.handleAddTodo()}
              >
               <View  >
                    <Text style={{alignItems:'center',alignSelf:'center',color:'#fff',margin:5}} >Submit</Text>
               </View>
               </TouchableOpacity>
                        {/* <TouchableOpacity onPress={()=>this.writeuserdata(this.state.name)} >
                        <Text style={{fontWeight:'bold',fontSize:20}} >
                            push
                        </Text>
                        </TouchableOpacity> */}
                    </View>
                    {/* <View style={{marginTop:'5%', alignItems: 'center', justifyContent: 'center', paddingBottom: '5%' }}>
                                            <TouchableOpacity
                                                // onPress={() => this.forgotValidation()}
                                                style={{
                                                    height: '6.5%', width:'35%', alignItems: 'center', justifyContent: 'center',
                                                    borderRadius: 5, backgroundColor: '#F05368'
                                                }}>
                                                <Text style={{ fontSize:'2%', color: 'white', fontFamily: 'OpenSans-Bold' }}>Submit</Text>
                                            </TouchableOpacity>
                                        </View> */}

</View>

</View>
                    }                 
{/* 
{this.state.isLoading ?
                    <View style={{ height: '100%', width: '100%', position: 'absolute', justifyContent: 'center', alignSelf: 'center' }}>
                        <ActivityIndicator
                            size={hp('6%')}
                            color={'#F05368'}
                            backgroundColor={'transparent'}
                        />
                    </View> : null} */}

</BottomSheet>
            </SafeAreaView>
        )
    }
}
// const mapStateToPops=(state)=>{
//     return{
//         toDoList:state.Todo_array
//     }

// }
// export default connect (mapStateToPops) (Home)
const mapStateToProps = (state, ownProps) => {
    return {
      todo_list: state.todos.todo_list,
    }
  }
  
  const mapDispatchToProps = { addTodo }
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Home)