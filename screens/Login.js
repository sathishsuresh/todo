import React from 'react'
import {View,Text, SafeAreaView,TextInput,TouchableOpacity} from 'react-native'
import {GoogleSignin,GoogleSigninButton,statusCodes} from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth'
import Toast from 'react-native-simple-toast';
export default class Login extends React.Component{

    componentDidMount(){
        GoogleSignin.configure({
            // scopes: ['email', 'profile'], // what API you want to access on behalf of the user, default is email and profile
            webClientId: '162014228232-dcbngotdjbuqn1po4ttlutgumipiek6i.apps.googleusercontent.com', //type3
            // androidClientId:"341155450362-r45315rmqmkoad41t6kka9m78nqp36vv.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
            // offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            // hostedDomain: '', // specifies a hosted domain restriction
            // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
            // forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
            // accountName: '', // [Android] specifies an account name on the device that should be used
            // iosClientId: "341155450362-k1rier6bn1n0t54h02rd9nhuu6pl65bi.apps.googleusercontent.com", // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
          }); 
    }

    
    signIn = async () => {
        try {
            
          await GoogleSignin.hasPlayServices();
          const userInfo = await GoogleSignin.signIn();
        //   alert("google data :" + JSON.stringify(userInfo))
          console.log("google data :" + JSON.stringify(userInfo))
          const {accessToken, idToken} = await GoogleSignin.signIn();      
      const credential = auth.GoogleAuthProvider.credential(
        idToken,
        accessToken,
      );
      await auth().signInWithCredential(credential);
          this.props.navigation.navigate('Home') 
          Toast.show("Login successfully!", Toast.SHORT)      
          this.setState({ userInfo });
          // const userInfo1 = await GoogleSignin.signOut();
        //   alert("google data :" + JSON.stringify(userInfo))
          // this.setState({ userInfo1});
        } catch (error) {
            console.log(error)
          if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            // user cancelled the login flow
          } else if (error.code === statusCodes.IN_PROGRESS) {
            // operation (e.g. sign in) is in progress already
          } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            // play services not available or outdated
          } else {
            // some other error happened
          }
        }
      }; 
    render(){
        return(
            <SafeAreaView style={{flex:1,backgroundColor:'#fff'}} >
            <View style={{ width: '100%', backgroundColor:'white' }}>
                        <Text style={{
                            fontSize: 30,
                            alignSelf: 'center', justifyContent: 'center', marginTop: 20,fontFamily:'OpenSans-Bold'
                        }}>
                            Log in
                        </Text>
                    </View>
                    <View style={{ width: '100%', height: 50, }}>
                        <View style={{
                            width: '80%', height: 50, backgroundColor: 'lightgrey',
                            alignSelf: 'center', justifyContent: 'center', marginTop: 20, borderRadius: 10
                        }}>
                            <TextInput style={{ marginLeft: 20, fontSize: 16 ,fontFamily:'OpenSans'}} placeholder="Email Id"
                                placeholderTextColor="black" 
                                //onChangeText={name => this.setState({ name })}
                                // value={this.state.name}
                                // onChangeText={(user_value) => this.login_email_validation(user_value)}
                                >

                            </TextInput>
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 50,marginTop:'5%' }}>
                        <View style={{
                            width: '80%', height: 50, backgroundColor: 'lightgrey',
                            alignSelf: 'center', justifyContent: 'center', marginTop: 20, borderRadius: 10
                        }}>
                            <TextInput style={{ marginLeft: 20, fontSize: 16 ,fontFamily:'OpenSans'}} placeholder="Password"
                                placeholderTextColor="black" 
                                secureTextEntry={true}
                                //onChangeText={name => this.setState({ name })}
                                // value={this.state.name}
                                // onChangeText={(user_value) => this.login_email_validation(user_value)}
                                >

                            </TextInput>
                        </View>
                    </View>
                    <View style={{ width: '100%', height: 50,marginTop:'5%' }}>
                        <TouchableOpacity >
                            <View style={{
                                width: '50%', height: 50, backgroundColor: '#e0416b',
                                alignSelf: 'center', justifyContent: 'center', marginTop: 20, borderRadius: 10
                            }}>
                                <Text style={{
                                    fontSize: 20, color: 'white', fontFamily: 'OpenSans-Bold',
                                    alignSelf: 'center', justifyContent: 'center'
                                }}>
                                    Log In
                            </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{alignContent:'center',justifyContent:'center',alignSelf:'center'}} >
                    <GoogleSigninButton
                    onPress={()=>this.signIn()}
    style={{ width: 192, height: 48,margin:'10%' }}
    size={GoogleSigninButton.Size.Wide}
    color={GoogleSigninButton.Color.Dark}
    // onPress={this._signIn}
    // disabled={this.state.isSigninInProgress}
     />
     </View>
                    </SafeAreaView>
        )
    }
}